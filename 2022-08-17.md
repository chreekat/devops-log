Weekly Log, weeks 13 and 14

*[This will be my last entry for six weeks. The next will come on September 30.]*

In the last week and a half, I continued to focus on spurious failures. To wrap things up before my break, I wrote a wiki page about the [spurious failure effort][wiki] that explains my methodology. I also added one more failure type ([MoveFileEx]). 

Outside of spurious failures, I made a performance improvement to CI by removing a heavyweight dependency ([#21492][21492]), explored the fragile test monitoring infrastructure, and spent some time dealing with another bout of sore throat and congestion. (Covid? Who even knows anymore.)

It's uncomfortably hot in *Finland* of all places. Hope you are all staying cool!

See you in a month!

-Bryan

[wiki]: https://gitlab.haskell.org/ghc/ghc/-/wikis/Tracking-Spurious-Failures
[21492]: https://gitlab.haskell.org/ghc/ghc/-/issues/21492
[MoveFileEx]: https://grafana.gitlab.haskell.org/d/167r9v6nk/ci-spurious-failures?orgId=2&from=now-90d&to=now&refresh=5m&var-types=MoveFileEx
