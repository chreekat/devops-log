It's the return of the weekly log!

The last month has been a whirlwind for me personally, so let's see how much of my work I can accurately recall.

Curiously, the very next task I recorded in my journal after completing the *last* weekly log was something that finally just reached a big milestone *yesterday*. Namely, the main body of the data migration process has finished! The process was mostly passive from my perspective, so it was actually a good time for me to be offline. I did have some issues with the migration tool that needed to be ironed out at first, but the migration itself took a week and a half. Unfortunately, it's still not quite done. There were a number of failures that need investigation. The failure rate was less than one in a million, and we may end up needing to accept it as the price of dealing with state in the real world. I haven't given up on those 161 objects yet, however.

Since the main migration process completed, I was able to address an issue that became known last week. Unbeknownst to me, stack used an s3.amazonaws.com URL by default to fetch snapshot versions. I had stopped updating the old bucket right before starting the data migration, which means the old URL had gone stale. Today, I wrote a little script to sync the snapshot data back to the old bucket, so nobody should be affected anymore. Stack will use an updated URL by default in the next release, and if you can't upgrade, you can change the default. See [snapshots] for details. The old bucket *will* eventually disappear, so everybody will need to use the new URL at some point. I don't yet know when that point might be. I'll add it to the list of followup tasks for the handover.

Another thing I worked on was a version of hackage-mirror-tool that uses amazonka rather than relying on a custom implementation of the AWS API. While I actually appreciate writing custom API clients in general because it reduces your exposure to churn you don't care about, hackage-mirror-tool was missing an implementation of AWS's SignatureV4 algorithm, which is the default for S3 and the *only* version supported by R2. I felt the world did not need another implementation of that algorithm. Plus, adapting the tool to use amazonka (which already does implement SignatureV4, of course) took less work overall.

I have only finished implementing and testing this new version of the tool — the code is not yet pushed anywhere and I haven't deployed it yet. I will be doing those things soon.

Besides these main tasks, there was the usual number of GHC CI issues that arose, mostly having to do with runner platforms being unreliable. I hope to address that more substantially after the Stackage handover is complete.

There were also a few Cabal CI issues and discussions I participated in, though most of the hard work is being done by the Cabal maintainers and release managers. I'm just giving my opinion where it is requested. :)

And yes, I have a summary of GHC issues triaged this week!

[details="GHC issues triaged this week"]

- [#24565: bindist configure fails when LDFLAGS contains pack-relative-locs](https://gitlab.haskell.org/ghc/ghc/-/issues/24565)
- [#24564: Types should be pretty printed as types, not via IfaceType](https://gitlab.haskell.org/ghc/ghc/-/issues/24564)
    - "sounds good"
- [#24563: IfaceAppArgs has inefficient Binary instance](https://gitlab.haskell.org/ghc/ghc/-/issues/24563)
    - really low hanging fruit
- [#24562: Hide Language.Haskell.TH.Lib.Internal from hoogle/haddocks](https://gitlab.haskell.org/ghc/ghc/-/issues/24562)
    - "we should do that"
    - also an easy thing to do.
- [#24561: &quot;Relocation out of range for SUBTRACTOR&quot; under aarch64_apple_darwin when using Template Haskell and the external interpreter](https://gitlab.haskell.org/ghc/ghc/-/issues/24561)
    - "What is SUBTRACTOR"
    - Should ghc just switch to the dynamic linker...?
- [#24559: Lambda with invisible type pattern fails to splice with TemplateHaskell](https://gitlab.haskell.org/ghc/ghc/-/issues/24559)
    - Ryan (reporter) is looking at it; SPJ is also
- [#24558: Haddock regression with respect to links of qualified identifiers](https://gitlab.haskell.org/ghc/ghc/-/issues/24558)
    - Fixed now. This was a duplicate
- [#24557: TypeAbstractions doesn&#39;t parse with \case, but does parse with \cases](https://gitlab.haskell.org/ghc/ghc/-/issues/24557)
    - It's a bug. Can the person who implented \cases look into it?
- [#24556: Constant folding integer division](https://gitlab.haskell.org/ghc/ghc/-/issues/24556)
    - Andreas will take another look
- [#24555: Test failures in 9.8.2 via test bindist](https://gitlab.haskell.org/ghc/ghc/-/issues/24555)
    - needs some info but bgamari is looking into it
- [#24554: ghc-9.10.0.20240313 (alpha1) does not install on macOS](https://gitlab.haskell.org/ghc/ghc/-/issues/24554)
    - Looks like a highest-priority packaging bug. romes will look into it.
- [#24553: Expected and actual kinds in &quot;Couldn&#39;t match kind&quot; error appear identical](https://gitlab.haskell.org/ghc/ghc/-/issues/24553)
    - there might already be a ticket for this. Ben is gonna look for it later
- [#24552: pattern synonyms changed semantics from 9.8 to 9.10?](https://gitlab.haskell.org/ghc/ghc/-/issues/24552)
    - Appears to be a bug introduced recently. Apoorv is looking at it.
- [#24551: Literal string redundantly abstracted over a type variable](https://gitlab.haskell.org/ghc/ghc/-/issues/24551)
    - Amazing repro by Krzysztof
    - Sebastian is possbily fixing it
- [#24550: NCG: Use 32bit-comparisons when operating on pointer tags.](https://gitlab.haskell.org/ghc/ghc/-/issues/24550)
    - a little optimization
- [#24549: &#39;&lt;stdin&gt;: hGetLine: invalid argument&#39; with Unicode input on Windows](https://gitlab.haskell.org/ghc/ghc/-/issues/24549)
    - winio helps a little but maybe not enough
- [#24548: configure ignores LDFLAGS](https://gitlab.haskell.org/ghc/ghc/-/issues/24548)
    - This sort of looks like a longstanding bug in ./configure, but, fixing it would be hard, and ghc-toolchain is primed to succeed ./configure within a few releases.
    - GHC has ungodly undocumented env vars that target specific stages of the compilation that... might be better? LDFLAGS is maybe kind of a shotgun approach?
    - But "you can't ignore standards" (I agree) and packagers do depend on LDFLAGS. Something needs to be done with it.
    - "We need a packaging guide", which reminds me again of my idea that we need some kind of "contract" or "API" between GHC and packagers. Trying to document it, and evolving reality and documentation until they agree with each other and are easy to use, is one experiment to try.
    - We need some fora for discussing potential changes, too. Even moving to "correct" behavior will break somebody somewhere.
- [#24545: GHC 9.10.1-alpha1 .tar.gz download does not extract properly](https://gitlab.haskell.org/ghc/ghc/-/issues/24545)
    - Embarrassing but fixed already, whew
- [#24544: Suggestion to add entity information to HieFile](https://gitlab.haskell.org/ghc/ghc/-/issues/24544)
    - Related to HLS
    - Already has a patch ready for review
- [#24543: improve documentation of :doc and -haddock](https://gitlab.haskell.org/ghc/ghc/-/issues/24543)
    - Some missing documentation.
    - Also has the idea to enable `-haddock` by default, which might actually be ok. It enables :doc in ghci. But need to check effects on compile time and space.
- [#24542: Windows binary distribution `settings` contain references to build environment paths](https://gitlab.haskell.org/ghc/ghc/-/issues/24542)
    - This issue affected the 9.10 alpha
- [#24541: Modify release process to upload to staging area](https://gitlab.haskell.org/ghc/ghc/-/issues/24541)
    - Since there's no automation for this, files can get pushed before the release is actually ready. Try to prevent that.
- [#24540: Deserialising `mi_extra_decls` introduces duplication](https://gitlab.haskell.org/ghc/ghc/-/issues/24540)
    - some kind of performance improvement by fendor
- [#24539: Improve memory efficiency of `UnlinkedBCO`](https://gitlab.haskell.org/ghc/ghc/-/issues/24539)
    - another performance improvement
- [#24538: Windows binary distributions ship with empty package database](https://gitlab.haskell.org/ghc/ghc/-/issues/24538)
    - Another windows packaging issue ready for review
- [#24537: template-haskell&#39;s RecordWildCards support](https://gitlab.haskell.org/ghc/ghc/-/issues/24537)
    - Looks like a bug, but it would be yet another TH breaking change to fix :( Maybe it can be done with care, but maybe it shouldn't be done just yet, either... The world needs new ways of doing these things.
    - [#24021: Making Template Haskell (more) stable across GHC releases](https://gitlab.haskell.org/ghc/ghc/-/issues/24021) was discussed
- [#24535: Allow foreign import declarations to be split into type definition and &quot;implementation&quot; import](https://gitlab.haskell.org/ghc/ghc/-/issues/24535)
    - It was discussed last week, looks cool, needs a GHC proposal
[/details]

[snapshots]: https://discourse.haskell.org/t/ann-stacks-default-source-for-list-of-stackage-snapshots-not-up-to-date/9023
