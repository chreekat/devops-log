Another week, another weekly log!

Last week, I *finally* finished migrating the "gitlab-storage" server. I am really grateful that I did so much preparation in advance, because it let me quickly resolve unexpected issues as they arose. And they did arise!

I'm proud of this accomplishment. Not only did I satisfy the critical operational need, I documented the crap out of the system and wrote a big checklist for the entire operation. The next time I or anybody else needs to do another migration, we won't have to start from scratch.

Sadly the servers repository is currently private, so I can't link to the docs yet (nor my sweary minute-by-minute log). But my long term plan is to bring that repo into the light, so one day the information will be free.

And now it's done! (Almost. I haven't destroyed the old server yet. But that should happen today. All other steps, really all of them, are done.)

**Moving on**. 

The migration took all day Thursady. Since then, I spent some time working on [bumping the minimum bootstrap compiler version][min-bootstrap]. This change would prevent an ugly error message that arises if people try to compile GHC with a version of GHC that's too old to do so. Unfortunately, I got blocked by CI issues (sigh) so I am unassigning myself for now.

I also spent a lot of time babysitting CI. Today, for example, I had to monitor the pipelines all day to deconflict some patches. On the plus side, one of the patches successfully [merged the JavaScript backend to master][js]! Now GHC 9.6 will have two new backends: WASM and JavaScript. In 9.6 they are just "feature previews" and lack some important production-quality touches, but it's a huge accomplishment and kudos are in order. They will be polished in upcoming releases.

Looking forward, the next thing on my own list with a deadline is [Mac notarization][mac]. And my overriding priority continues to be removing friction for GHC contributors by making CI more reliable.

[mac]: https://gitlab.haskell.org/ghc/ghc/-/issues/17418
[js]: https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9133
[min-bootstrap]: https://gitlab.haskell.org/ghc/ghc/-/issues/22342
