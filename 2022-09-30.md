Welcome back! This is Week 15 of the Haskell Foundation DevOps update, my first week back on the job after spending a month doing a very different kind of system maintenance.

This week, we decided I would next work on Mac notarization and the observability of fragile test metrics. But I spent much of the week on administrative tasks and coordination, and I'm not yet done with spurious CI failures.

* I will be attending MuniHac and NixCon in person, so I arranged transport and accommodation (and I hope to see you there!). 
* Certain Equinix servers used by our GitLab are being retired, and there was some synchronization about what is at stake and who needs to do what.
* I had to catch up on a 5-week email backlog.
* CI jobs were running out of memory for a fixable reason... which was fixed.
* I took care of a lot of little administrative tasks, using the *"organize your todo list by doing the things on the list"* approach.
* Read a few articles about Mac notarization to learn what that even means.

Coming up, I will be scheduling and executing the server migration, tweaking the Spurious Failure dashboard based on user feedback, clearing out my list of suspicious failures (do they represent a class of failures that should be tracked?), and then finally moving on with Mac notarization.

As you can see, my overall mission will continue to be helping improve GHC CI. There are many ways the Haskell community could use my time, and some day my mission will switch to one of those other ways. But not yet. (I intend to revisit this every three months.)
