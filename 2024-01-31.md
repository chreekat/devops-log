It's the weekly log!

The last week had some variety due to competing deadlines and emergent issues. In terms of issues, I had three different things to deal with. First, I wrapped up my recent work with mitigating spurious failures in GHC CI, which had gotten unbearable lately. Things are looking better now:

![image|690x339](upload://mFyMYdrDls3lV0xhj96urcXuqEO.png)

(The last week's numbers are better for job failures and master-branch pipeline failures! But still not great, obviously...)

Then, I got a couple reports of trouble receiving email from gitlab.haskell.org. I was able to track down the reason for both of them. I was pretty happy to be able to fix one of them by emailing the admins for icloud.com, who addressed our issue in under a couple hours! I'm still kind of amazed by that. Thank you to whoever made that happen. The other email issue wasn't so easily resolved. In fact, it still isn't resolved. :/ Email is hard.

The third emergent issue was the discovery that a bunch of macOS CI runners had become unresponsive. Luckily I was still able to log in and clean things up.

...But hey, look at that. I just went to check on the runners again, and a *different* set of macOS runners are now unresponsive. I really can't wait to have the time to fix these issues properly.

Luckily I did still have time for my main task, which is the migration of Stackage services to Haskell Foundation servers. I wrote https://github.com/commercialhaskell/stackage/pull/7299, which adds support for the new Haddock bucket, and began preparing to run a new Hackage mirror that will replace the long-serving mirror hosted by FP Complete. Last but not least, I drafted a project to include in https://summer.haskell.org/ideas.html that I hope to have posted by tomorrow.

That's all for now!

And here's the list of issues triaged yesterday. Like usual, I could not stay till the end of the call, so my notes are incomplete.


[details="GHC Issue Triage Notes"]

- [#24369: CI: ensure more tests are run using LLVM](https://gitlab.haskell.org/ghc/ghc/-/issues/24369)
    - A bunch of hard work on CI to enable testing more specific combinations of the huge matrix of combinations
- [#24370: SPECIALISE rule desugaring fails unnecessarily](https://gitlab.haskell.org/ghc/ghc/-/issues/24370)
    - Has a fix  already
- [#24371: Wanted: better arity analysis](https://gitlab.haskell.org/ghc/ghc/-/issues/24371)
    - Some potential improvement in the optimizer
- [#24372: Annoying but possibly harmless errors in test log](https://gitlab.haskell.org/ghc/ghc/-/issues/24372)
    - not sure what's going on here, but it seems harmless
- [#24373: ghcjs: Intermittent failures in test process005](https://gitlab.haskell.org/ghc/ghc/-/issues/24373)
    - being worked on
- [#24374: GHC 9.10.1 tracking ticket](https://gitlab.haskell.org/ghc/ghc/-/issues/24374)
    - work in progress!!
- [#24376: Template Haskell not working with WASM backend](https://gitlab.haskell.org/ghc/ghc/-/issues/24376)
    - yes.
- [#24377: Wrong type signature for JavaScript asyncCallback in document](https://gitlab.haskell.org/ghc/ghc/-/issues/24377)
    - being handled by js team
- [#24380: Build alpine bindists on more recent alpine release (3.18)](https://gitlab.haskell.org/ghc/ghc/-/issues/24380)
    - will be added to the exsiting set
- [#24382: Hadrian &quot;resource busy&quot; errors are more frequent](https://gitlab.haskell.org/ghc/ghc/-/issues/24382)
    - not in ci, but in local VMs it seems pretty common there
    - Zubin sees it as well in HLS CI, and in many Windows versions
    - Maybe something changed in Windows?
- [#24383: T24171 segfaults on AArch64 with GHC HEAD:](https://gitlab.haskell.org/ghc/ghc/-/issues/24383)
    - unfortunately --flavour=release is not supported(?) 
    - Because split-sections isn't supported on aarch64, and release enables it, so on aarch64 you have to run --flavour=release+no_split_sections
    - some push back on whether or not "release" should mean "release" or if it means a specific set of arguments that may or may not (in fact, does not) work on every platform.
    - Basically just need to treat release as "release_base" because that's what it is. But we might be stuck with the name now.
    - Still need to double check that it's not an actual bug unrelated to split sections.
- [#24384: Support for external build system from MkDepend mode, i.e. ghc -M or equivalent](https://gitlab.haskell.org/ghc/ghc/-/issues/24384)
    - Related to similar past attempts
    - More thought needed perhaps, and clarification of what the goals are and how they are achieved
- [#24385: Bootstrapping GHC via c](https://gitlab.haskell.org/ghc/ghc/-/issues/24385)
    - Would be nice if it was a possibility
    - In particular, the idea is to compile GHC to C, which can be moved to another system and built there. But it has been broken for a while.
    - Maybe not that difficult to implement!?
- [#24386: Provide a way to desugar without inlining in the GHC API](https://gitlab.haskell.org/ghc/ghc/-/issues/24386)
    - Already quite a lot of discussion
    - The use case is LiquidHaskell
    - May end up being a simple flag
- [#24387: Hadrian uses deprecated way to set project file.](https://gitlab.haskell.org/ghc/ghc/-/issues/24387)
    - Ben is already working on it, but it looks like it can't be fixed due to backward/forward compat issues with Cabal
- [#24388: Argument size computation for foreign ffi exports is likely wrong.](https://gitlab.haskell.org/ghc/ghc/-/issues/24388)
    - Seems to be a problem that will affect very few people
- [#24389: error: ld: unknown option: -no_warn_duplicate_libraries on macOS with GHC 9.6.4](https://gitlab.haskell.org/ghc/ghc/-/issues/24389)
    - There's some trouble getting this to duplicate. Matt will attempt.
- [#24390: Implement multiline strings](https://gitlab.haskell.org/ghc/ghc/-/issues/24390)
    - I'm excited about this
- [#24391: Add hadrian option for rerunning failed test](https://gitlab.haskell.org/ghc/ghc/-/issues/24391)
    - If someone wants to do it, go for it
- [#24392: GHC 9.8.1 crashes with `heap overflow` while deriving `Data` on Windows if both `-O -XHaskell2010` are enabled](https://gitlab.haskell.org/ghc/ghc/-/issues/24392)
    - Very interesting.
    - Needs more data
- [#24393: Using `-Dn` can lead to segfaults](https://gitlab.haskell.org/ghc/ghc/-/issues/24393)
- [#24394: `-Dn` is missing from user guide](https://gitlab.haskell.org/ghc/ghc/-/issues/24394)
- [#24395: Profiled GHC flavour segfaults when building a target project with TH or GHC plugin](https://gitlab.haskell.org/ghc/ghc/-/issues/24395)
- [#24396: Namespacing for WARNING/DEPRECATED pragmas](https://gitlab.haskell.org/ghc/ghc/-/issues/24396)

[/details]
